// Title Component   
class TitleComponent extends HTMLElement {
    
    template = HTMLElement.prototype

    mounted = null;

    // Elements
    title_wrapper_mobile = HTMLElement.prototype
    title_wrapper_desktop = HTMLElement.prototype
    title_mobile = HTMLElement.prototype
    title_desktop = HTMLElement.prototype
    
    // Atributes

    // Check now our new super SVG Colorizer. It is free! (Desktop)

    _titleMobile = 'default title'
    _titleDesktop = 'default title'

    constructor() {
        super();

        this.template = document.createElement("template");

        this.template.innerHTML = `
            <style>
            .title__container--desktop {
                text-align: center;
                height: 60px;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            .title__container--desktop a {
                font-size: 15px;
                color: #878787;
                text-decoration: none;
                transition: all .5s ease;    
            }

            .title__container--desktop a:hover {
                color: #6622D6;
                animation: title .2s;    
            }

            .title__container--mobile {
                display: none;
            }

            .title__container--mobile a{
                font-size: 12px;
                height: 60px;
                color: #878787;
                display: flex;
                justify-content: center;
                align-items: center;
                text-decoration: none;
            }
    
            .subscribe__text--mobile{
                margin: 0 .5em;
            }

            @media only screen and (max-width: 800px) {
                .title__container--desktop {
                    display: none;
                }
                .title__container--mobile {
                    display: inline;
                }
            }
            </style>
            `;


        // Create Elements

        // Title mobile
        this.title_wrapper_mobile = document.createElement('div');
        this.title_wrapper_mobile.classList.add('title__container--mobile');
        this.title_mobile = document.createElement('a');
        this.title_mobile.setAttribute('href', 'http://www.bypeople.com');

        // Title desktop
        this.title_wrapper_desktop = document.createElement('div');
        this.title_wrapper_desktop.classList.add('title__container--desktop')
        this.title_desktop = document.createElement('a');
        this.title_desktop.classList.add('title_desktop')
        this.title_desktop.setAttribute('href', 'http://www.bypeople.com');


        // TAKE ATTRIBUTE
        this.title_mobile.innerText = this._titleMobile;
        this.title_desktop.innerText = this._titleDesktop;


        // EXPORT THE TEMPLATE
        this.title_wrapper_mobile.appendChild(this.title_mobile);
        this.title_wrapper_desktop.appendChild(this.title_desktop);

        this.attachShadow({ mode: "open" });
        this.shadowRoot.innerHTML = this.template.innerHTML;
        this.shadowRoot.appendChild(this.title_wrapper_mobile);
        this.shadowRoot.appendChild(this.title_wrapper_desktop);


        this.title_mobile.innerText = this._titleMobile;
        this.title_desktop.innerText = this._titleDesktop;

        this._desk = this.title_wrapper_desktop.querySelector(".title_desktop")
    }

    connectedCallback(){
        console.log('este es el titulo actual: ' + this.title_desktop.innerText)
        console.log(this._titleDesktop)
        
    }

    setState(globalComp){

        var _newDesk = null;
        var _newMobile = null;
        
        this._nd = globalComp.titleDesktop

        if (globalComp.titleDesktop) {
            _newDesk = globalComp.titleDesktop
            console.log(_newDesk)
        }
    
        if (globalComp.titleMobile) {
            _newMobile = globalComp.titleMobile
        }

        if (_newDesk) {
            console.log(this.shadowRoot.querySelector(".title_desktop"))
            this._desk.textContent = _newDesk
            return false;
        }

        // console.log(this._desk.textContent)

        return true;
    }

    // set nuevoTitulo(value){
    //     this.new_title = globalComp.titleDesktop
    // }
    
    // get nuevoTitulo(){
    //     return this.new_title
    // }
    
}

//SubscribeComponent 
class SubscribeComponent extends HTMLElement {  

    template = HTMLElement.prototype
    
    input_wrapper = HTMLDivElement.prototype;
    subscribe_text = HTMLParagraphElement.prototype;
    subscribe_input = HTMLInputElement.prototype;
    subscribe_button = HTMLButtonElement.prototype;
    subscribe_message = HTMLParagraphElement.prototype;


    _message_default = "default message"

    /* Setting props */
    constructor() {

        super();

        this.template = document.createElement("template");

        this.template.innerHTML = `
            <style>

            /**/
            .subscribe__container{
                height: 60px;
                color: #878787;
                display: flex;
                align-items: center;
                justify-content: space-around;
            }


            /* Container Input */

            .input__container {
                display: flex;
                align-items: center;
            }

            /* Text */
            .subscribe__text {
                margin: 0;
                font-size: 90%;
                color: #6C6C6C;
                transition: all 5s ease;
            }

            .subscribe__text.has-error {
                margin: 0;
                font-size: 90%;
                color: #BF0101;
                transition: all 1s ease;
            }

            .subscribe__text.has-success {
                margin: 0;
                font-size: 90%;
                color: #06850A;
                transition: all .6s ease;
            }
            

            /* Input */
            .subscribe__input  {
                margin: 0 .5em;
                min-height: 30px;
                border: none;
                border-radius: 5px;
                transition: border-color .5s ease-out;
                padding: 0 10px;
                outline: 0;
                border: 3px solid #fff;
                font-family: 'Montserrat', sans-serif;
            }

            .subscribe__input:hover {
                border: 3px solid #8842d5;
            }

            .subscribe__input:focus {
                border: 3px solid #17EFD8;
                
            }

            .subscribe__input.has-error {
                border: 3px solid #BF0101;
                background: #F1D6D6;
                color: #BF0101;
                transition: all .3s ease;
            }

            .subscribe__input.has-success {
                border: 3px solid #00500B;
                background: #DBEEDC;
                color: #06850A;
            }

            
            .subscribe__submit{
                font-size: 12px;
                font-family: 'Montserrat', sans-serif; 
                background-color: #6622D6;
                border: none;
                border-radius: 6px;
                color: #17EFD8;
                padding: 9px 16px;
                outline: 0;
                cursor: pointer;
                font-weight: bold;
                letter-spacing: 2px;
                transition: all .5s ease;
            }

            .subscribe__submit.error{
                font-size: 12px;
                font-family: 'Montserrat', sans-serif; 
                background-color: #F1D6D6;
                border: none;
                border-radius: 6px;
                color: #BF9F9F;
                padding: 9px 16px;
                outline: 0;
                cursor: pointer;
                font-weight: bold;
                letter-spacing: 2px;
                transition: all .5s ease;
            }

            .subscribe__submit:hover{
                color: #6622D6;
                background: #17EFD8;
                transition: all .3s ease;
            }
            
            .subscribe__close{
                height: 20px;
                cursor: pointer;
            }

            .subscribe__close:hover{
                height: 22px;
            }

            .subscribe__close div {
                width: 20px;
                height: 2px;
                background-color: #878787;
                margin: 4px;
                border-radius: 10em;
                transition: all 0.3s ease;
            }

            .subscribe__close--line1 {
                transform: rotate(-45deg) translate(-7px,1px);
            }

            .subscribe__close--line2 {
                transform: rotate(45deg) translate(-3px,3px);
            }

            
            @keyframes submit {
                from {background-color: var(--secondary-color);}
                to {background-color: var(--hover-color);}
            }


            @media only screen and (max-width: 800px) {
                .subscribe__text--mobile, .subscribe__input--mobile, .subscribe__close--mobile{
                    display: none;
                }
            }
            </style>
        `;

        this.subscribe_wrapper = document.createElement('div');
        this.subscribe_wrapper.setAttribute('class', 'subscribe__container')

        this.input_wrapper = document.createElement('div');
        this.input_wrapper.classList.add('input__container')
        this.input_wrapper.setAttribute('id', 'input__container')

        this.subscribe_text = document.createElement('p');
        this.subscribe_text.setAttribute('class', 'subscribe__text')

        this.subscribe_input = document.createElement('input');
        this.subscribe_input.setAttribute('class', 'subscribe__input')
        this.subscribe_input.setAttribute('type', 'email')
        this.subscribe_input.setAttribute('placeholder', 'name@domail.com')

        this.subscribe_button = document.createElement('button');
        this.subscribe_button.classList.add('subscribe__submit')
        this.subscribe_button.setAttribute('type', 'submit')



        this.subscribe_close = document.createElement('div');
        this.subscribe_close.setAttribute('class', 'subscribe__close')
        this.subscribe_close_line1 = document.createElement('div');
        this.subscribe_close_line1.setAttribute('class', 'subscribe__close--line1')
        this.subscribe_close_line2 = document.createElement('div');
        this.subscribe_close_line2.setAttribute('class', 'subscribe__close--line2')


        // TAKE ATTRIBUTE

        this._inputSubscribe = this.getAttribute('subscribe__button');
        this.subscribe_button.innerText = "SUBSCRIBE";
        this._inputContainer = this.input_wrapper.querySelector(".input__container")
        
        
        // EXPORT THE TEMPLATE
        this.subscribe_wrapper.appendChild(this.input_wrapper);
        this.input_wrapper.appendChild(this.subscribe_text);
        this.input_wrapper.appendChild(this.subscribe_input);
        this.input_wrapper.appendChild(this.subscribe_button);
        this.subscribe_close.appendChild(this.subscribe_close_line1);
        this.subscribe_close.appendChild(this.subscribe_close_line2);
        this.subscribe_wrapper.appendChild(this.subscribe_close)

        this.attachShadow({ mode: "open" })
        this.shadowRoot.innerHTML = this.template.innerHTML;
        this.shadowRoot.appendChild(this.subscribe_wrapper)

        this.subscribe_text.innerText = this._message_default;
        
        
        this._message = this.subscribe_wrapper.querySelector(".subscribe__text") 
        this._input = this.subscribe_wrapper.querySelector("input")
        this._submit = this.subscribe_wrapper.querySelector("button")
        this._inputContainer = this.subscribe_wrapper.querySelector(".input__container")
        
    }

    connectedCallback() {
        
        // console.log('The subscribe component is mounted', this.subscribe_wrapper)
        this._input.addEventListener("focus", this.handleOnFocus)
        this._input.addEventListener("keydown", this.handleKeyDown)
        this._submit.addEventListener("click", this.handleOnClick)
        this._input.addEventListener("paste", this.handlePaste);

    }


    disconnectedCallback() {
        this._input.removeEventListener("keydown", this.handleKeyDown)
        this._input.removeEventListener("paste", this.handlePaste)
    }


    handleKeyDown = evt => {
        this._input.classList.remove("has-error");
        this._input.classList.remove("has-success")
        this._message.classList.remove("has-error")
        this._message.classList.remove("has-success")


        const TRIGGER_KEYS = ["Enter", "Tab", ","];
        
        if(TRIGGER_KEYS.includes(evt.key)) {
            evt.preventDefault();

            var value = evt.target.value.trim();

            if(value && this.validate(value)) {

                this._email = evt.target.value

            }
        }
    }

    handleOnClick = evt => {

        this._input.classList.remove("has-error");
        this._input.classList.remove("has-success")
        this._message.classList.remove("has-error")
        this._message.classList.remove("has-success")

        
        var value = this._input.value

        if(value && this.validate(value)){
            // this._inputContainer.setAttribute('hidden')
        } else {
            
        }   
    }
   

    validate(email) {
        var message_error = null;
        var message_success = null;

        if(!this.isEmail(email)) {
            message_error = `Email invalid`
        }
        
        if(this.isEmail(email)){
            message_success = `Email valid`
        }

        if (message_error) {
            this._message.textContent = message_error;
            this._message.classList.add('has-error');
            this._input.classList.add('has-error');
            this.subscribe_button.classList.add('error');
            
            
            let restoreMessage = () => {
                this._message.textContent = this._message_default
                this._message.classList.remove('has-error');
                this._input.classList.remove('has-error');
                this.subscribe_button.classList.remove('error');
            }

            setTimeout(restoreMessage, 2000)
             
            return false;
        }

        if (message_success) {
            this._message.textContent = message_success;
            this._message.removeAttribute('hidden')
            this._input.classList.add('has-success')
            this._message.classList.add('has-success');

            // remove after 3s
            let restoreMessage = () => {
                this._inputContainer.setAttribute('hidden')
            }

            setTimeout(restoreMessage, 5000)
            
            return true
        }

        

        return true;
    }

    isEmail(email) {
       return /[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/.test(email);
    }  
}


// Main Component
class TopbarComponent extends HTMLElement {

    template = HTMLElement.prototype

    stateGlobal = null;
    
    constructor() {
        super();

        this.template = document.createElement("template");

        this.template.innerHTML = `
            <style>
            @import url('https://fonts.googleapis.com/css?family=Kulim+Park|Montserrat&display=swap');
            :host {
                --primary-color: #212121;
                --secondary-color: #980093; 
                --hover-color: #0A8A9E;
                --text-color: #FFFFFF;
            }
            .topbar-component {
                min-height: 60px;
                width: 100%;     
                background: #F9F9F9;
                display: flex;
                justify-content: space-around;
            }
            a{
                font-size: 30px;
            }
            .title-component{
                width: 50%
            }       
            .subscribe-component{
                width: 50%
            }     
            </style>    
        `;

        this.divGlobal = document.createElement('div');
        this.divGlobal.setAttribute('class', 'topbar-component')

        this.titleComponent = document.createElement("title-component");
        this.titleComponent.setAttribute('class', 'title-component')


        this.subscribeComponent = document.createElement("subscribe-component");
        this.subscribeComponent.setAttribute('class', 'subscribe-component')

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = this.template.innerHTML;
        this.divGlobal.appendChild(this.titleComponent);
        this.divGlobal.appendChild(this.subscribeComponent);
        this.shadowRoot.appendChild(this.divGlobal);
        

        // this.subscribe_text.innerText = this._message_default;
        
        
        // this._message = this.subscribe_wrapper.querySelector(".subscribe__text") 
        // this._input = this.subscribe_wrapper.querySelector("input")
        // this._submit = this.subscribe_wrapper.querySelector("button")
        // this._inputContainer = this.subscribe_wrapper.querySelector(".input__container")

        // this._titleDesktop = this.titleComponent.title_wrapper_desktop.querySelector(".title_desktop")
    }


    connectedCallback() {
        console.log(this._titleDesktop)
    }

    // setState(globalComp){
        
    //     this.new_title = globalComp.titleDesktop
    //     if(this.new_title) {
    //         this._titleDesktop.textContent = this.new_title
    //     }
             
    //     console.log(this._titleDesktop)
    // }

    // get newTitle(){
    //     return this._titleDesktop;
    // }

    // set newTitle(value) {
    //     value = this.new_title
    //     this._titleDesktop = value;
    // }
}


window.customElements.define('subscribe-component', SubscribeComponent);
window.customElements.define('title-component', TitleComponent);
window.customElements.define('topbar-component', TopbarComponent);


// var globalComp = document.createElement("topbar-component");
var globalComp = document.createElement("title-component");


globalComp.setState({
    titleDesktop: 'the new title',
    titleMobile: 'the new desktop'
})